package dao;

import model.Bid;
import model.Item;
import model.User;
import model.exception.InvalidBidException;

import java.util.List;
import java.util.PriorityQueue;

public interface BidTracker {
    /**
     * @param bid Consists of a User(non-nullable), an Item(non-nullable) and a Price.
     * @throws InvalidBidException when Price is lower than current winning price
     * @throws InvalidBidException when Price is set to zero
     * @throws NullPointerException when Bid, Bid.User or Bid.Item are set to null
     */
    void placeBid(Bid bid) throws InvalidBidException;

    /**
     * @param item Consists of an ID and a name
     * @return Bid object having User, Item and Price
     * @throws NullPointerException if item is null
     */
    Bid getCurrentWinningBidForItem(Item item);

    /**
     * @param item Consists of an ID and a name
     * @return a priority queue with the highest bid at the top of the queue and the lowest at the bottom
     * @throws NullPointerException if item is null
     */
    PriorityQueue<Bid> getAllBidsForItem(Item item);

    /**
     * @param user Consists of an ID and a name
     * @return a list of items the user has bid on. Historical view, from first item bid on to last.
     * @return
     */
    List<Item> getAllItemsUserHasBidOn(User user);
}
