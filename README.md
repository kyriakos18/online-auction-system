# online-auction-system
## Use Cases
1. Place Bid
2. Get the current winning bid for an item
3. Get all the bids for an item
4. Get all the items on which a user has bid

## Design Decisions
1. Bidding starts at price > 0
2. Prices are represented in pence using Integer, to avoid any precision issues
3. Object Oriented Approach using POJOs for Bid, User and Item

## How to run tests
1. mvn test
