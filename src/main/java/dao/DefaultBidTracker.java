package dao;

import Utils.BidComparator;
import model.Bid;
import model.Item;
import model.User;
import model.exception.InvalidBidException;
import java.util.*;

public class DefaultBidTracker implements BidTracker {

    private Map<User,List<Item>> userBids;
    private Map<Item, PriorityQueue<Bid>> itemBids;

    public DefaultBidTracker(){
        userBids = new HashMap<>();
        itemBids = new HashMap<>();
    }

    @Override
    public void placeBid(Bid bid) throws InvalidBidException {
        checkBidForNulls(bid);
        if (bid.getPrice() <= 0 ){
            throw new InvalidBidException(String.format("Your Bid: £%s. Bids must be greater than £0", bid.getPriceInPounds()));
        }
        Bid currentWinningBid = getCurrentWinningBidForItem(bid.getItem());
        if (currentWinningBid == null){
            PriorityQueue<Bid> bidQueue = new PriorityQueue<>(new BidComparator());
            bidQueue.add(bid);
            itemBids.put(bid.getItem(), bidQueue);
            addUserBid(bid.getUser(), bid.getItem());
        } else {
            if (currentWinningBid.getPrice() < bid.getPrice()) {
                PriorityQueue<Bid> bidQueue = itemBids.get(bid.getItem());
                bidQueue.add(bid);
                itemBids.replace(bid.getItem(), bidQueue);
                addUserBid(bid.getUser(), bid.getItem());
            } else {
                throw new InvalidBidException(String.format("For %s, Highest Bid: £%s. Your Bid: £%s. " +
                                "New bids should be higher than current winning bid.", bid.getItem(),
                                currentWinningBid.getPriceInPounds(), bid.getPriceInPounds()));
            }
        }
    }

    @Override
    public Bid getCurrentWinningBidForItem(Item item) {
        return itemBids.get(Objects.requireNonNull(item)) == null ? null : itemBids.get(item).peek();
    }

    @Override
    public PriorityQueue<Bid> getAllBidsForItem(Item item) {
        return itemBids.getOrDefault(Objects.requireNonNull(item), new PriorityQueue<>());
    }

    @Override
    public List<Item> getAllItemsUserHasBidOn(User user) {
        return userBids.getOrDefault(Objects.requireNonNull(user), Collections.emptyList());
    }

    private void addUserBid(User user, Item item){
        if (userBids.containsKey(user)){
            List<Item> itemList = userBids.get(user);
            itemList.add(item);
            userBids.replace(user, itemList);
        } else {
            List<Item> itemList = new ArrayList<>();
            itemList.add(item);
            userBids.put(user, itemList);
        }
    }

    private void checkBidForNulls(Bid bid){
        Objects.requireNonNull(bid);
        Objects.requireNonNull(bid.getItem());
        Objects.requireNonNull(bid.getUser());
    }
}