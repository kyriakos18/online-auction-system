package Utils;

import model.Bid;

import java.util.Comparator;

public class BidComparator implements Comparator<Bid> {
    public int compare(Bid bid1, Bid bid2) {
        return bid2.getPrice() - bid1.getPrice();
    }
}
