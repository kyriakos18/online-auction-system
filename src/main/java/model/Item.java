package model;

import java.util.Objects;

public class Item {
    private final long id;
    private final String name;

    public Item (long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (id != item.id) return false;
        return (!name.equals(item.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return String.format("Item { id: %s, name: %s }", id, name);
    }

}
