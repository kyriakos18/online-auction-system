package model;

import java.util.Comparator;
import java.util.Objects;

public class Bid {
    private final User user;
    private final Item item;
    private final int price;

    public Bid (User user, Item item, int price) {
        this.user = user;
        this.item = item;
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public Item getItem() {
        return item;
    }

    public int getPrice() {
        return price;
    }

    public double getPriceInPounds() {
        return (double) price / 100;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bid bid = (Bid) o;

        if (user != bid.user) return false;
        if (!item.equals(bid.item)) return false;
        return price == bid.price;

    }

    @Override
    public int hashCode() {
        return Objects.hash(user, item, price);
    }

    @Override
    public String toString() {
        return String.format("Bid{ User: %s, Item: %s, Price(p): %s, Price(£): %s}", user, item, price, getPriceInPounds());
    }
}

