import dao.DefaultBidTracker;
import model.Bid;
import model.Item;
import model.User;
import model.exception.InvalidBidException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.*;

public class DefaultBidTrackerTest {

    private DefaultBidTracker bidTracker;
    private final User user1 = new User(1L, "Kyriakos Pieris");
    private final User user2 = new User(2L, "Matthew OReilly");
    private final Item item1 = new Item(1L, "MacBook Air");
    private final Item item2 = new Item(1L, "ChromeBook");

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void initiateBidTracker(){
        bidTracker = new DefaultBidTracker();
    }

    @Test
    public void placeBidShouldAddBidToUserAndItemBidMapsWhenBidIsValid() throws InvalidBidException {
        Bid validBid = new Bid(user1, item1, 1000);
        bidTracker.placeBid(validBid);
        PriorityQueue<Bid> expectedBidQueue = bidTracker.getAllBidsForItem(item1);
        assertEquals(expectedBidQueue.peek(), validBid);
        List<Item> actualItemBidList = bidTracker.getAllItemsUserHasBidOn(user1);
        assertEquals(Collections.singletonList(validBid.getItem()), actualItemBidList);
    }

    @Test
    public void placeBidShouldAddSecondBidToUserAndItemBidMapsWhenSecondBidIsValidAndAddToTopOfQueue() throws InvalidBidException {
        Bid firstBid = new Bid(user1, item1, 1000);
        Bid secondBid = new Bid(user2, item1, 2000);
        bidTracker.placeBid(firstBid);
        bidTracker.placeBid(secondBid);
        PriorityQueue<Bid> actualItemBidQueue = bidTracker.getAllBidsForItem(item1);
        assertEquals(2, actualItemBidQueue.size());
        assertEquals(secondBid, actualItemBidQueue.peek());
    }

    @Test
    public void placeBidShouldThrowInvalidBidExceptionWhenBidIsLowerThanWinningBid() throws InvalidBidException {
        Bid validBid = new Bid(user1, item1, 1000);
        Bid invalidBid = new Bid(user2, item1, 156);
        bidTracker.placeBid(validBid);
        exception.expect(InvalidBidException.class);
        exception.expectMessage("For Item { id: 1, name: MacBook Air }, Highest Bid: £10.0. Your Bid: £1.56. "
                + "New bids should be higher than current winning bid.");
        bidTracker.placeBid(invalidBid);
    }

    @Test
    public void placeBidShouldThrowInvalidBidExceptionWhenBidIsEqualToZero() throws InvalidBidException {
        Bid invalidBid = new Bid(user1, item1, 0);
        exception.expect(InvalidBidException.class);
        exception.expectMessage("Your Bid: £0.0. Bids must be greater than £0");
        bidTracker.placeBid(invalidBid);
    }

    @Test
    public void placeBidShouldThrowInvalidBidExceptionWhenBidIsLessThanZero() throws InvalidBidException {
        Bid invalidBid = new Bid(user1, item1, -1000);
        exception.expect(InvalidBidException.class);
        exception.expectMessage("Your Bid: £-10.0. Bids must be greater than £0");
        bidTracker.placeBid(invalidBid);
    }

    @Test (expected = InvalidBidException.class)
    public void placeBidShouldThrowInvalidBidExceptionWhenBidIsEqualToTheWinningBid() throws InvalidBidException {
        Bid validBid = new Bid(user1, item1, 1000);
        Bid invalidBid = new Bid(user2, item1, 1000);
        bidTracker.placeBid(validBid);
        bidTracker.placeBid(invalidBid);
    }

    @Test(expected = NullPointerException.class)
    public void placeBidShouldThrowNullPointerExceptionWhenBidIsNull() throws InvalidBidException {
        bidTracker.placeBid(null);
    }

    @Test(expected = NullPointerException.class)
    public void placeBidShouldThrowNullPointerExceptionWhenUserInBidIsNull() throws InvalidBidException {
        bidTracker.placeBid(new Bid(null, item1, 10));
    }

    @Test(expected = NullPointerException.class)
    public void placeBidShouldThrowNullPointerExceptionWhenItemInBidIsNull() throws InvalidBidException {
        bidTracker.placeBid(new Bid(user1, null, 10));
    }

    @Test
    public void placeBidOnItemShouldBeAbleToContinueAddingBidsEvenWhenSomeAreInvalid() throws InvalidBidException {
        Bid validBid1 = new Bid(user1, item1, 20);
        Bid validBid2 = new Bid(user2, item1, 30);
        bidTracker.placeBid(validBid1);
        bidTracker.placeBid(validBid2);
        try {
            Bid invalidBid = new Bid(user2, item1, 25);
            bidTracker.placeBid(invalidBid);
        } catch (InvalidBidException ex) {
            //log something
        }
        Bid validBid3 = new Bid(user1, item1, 35);
        bidTracker.placeBid(validBid3);
        PriorityQueue<Bid> actualItemBidQueue = bidTracker.getAllBidsForItem(item1);
        assertEquals(3, actualItemBidQueue.size());
        assertEquals(validBid3, actualItemBidQueue.peek());
    }

    @Test
    public void getCurrentWinningBidForItemShouldReturnTheBidWithMaximumPriceForThatItem() throws InvalidBidException{
        Bid validBid1 = new Bid(user1, item1, 20);
        Bid validBid2 = new Bid(user2, item1, 40);
        bidTracker.placeBid(validBid1);
        bidTracker.placeBid(validBid2);
        assertEquals(validBid2, bidTracker.getCurrentWinningBidForItem(item1));
    }

    @Test(expected = NullPointerException.class)
    public void getCurrentWinningBidForItemShouldThrowNullPointerExceptionWhenItemisNull(){
        bidTracker.getCurrentWinningBidForItem(null);
    }

    @Test
    public void getCurrentWinningBidForItemShouldReturnNullWhenNoBidsHaveBeenMadeForItem(){
        assertNull(bidTracker.getCurrentWinningBidForItem(item1));
    }

    @Test
    public void getAllBidsForItemAhouldReturnEmptyQueueWhenItemHasNoBids() {
        PriorityQueue<Bid> bids = bidTracker.getAllBidsForItem(item1);
        assertTrue(bids.isEmpty());
    }

    @Test
    public void getAllBidsForItemShouldReturnTheCorrectListOfBidsWhenItemHasBids() throws InvalidBidException {
        Bid validBid1 = new Bid(user1 , item1, 1000);
        Bid validBid2 = new Bid(user1 , item1, 2000);
        bidTracker.placeBid(validBid1);
        bidTracker.placeBid(validBid2);
        PriorityQueue<Bid> actualItemBids = bidTracker.getAllBidsForItem(item1);
        List<Bid> expectedBids = Arrays.asList(validBid2, validBid1);
        assertEquals(expectedBids, new ArrayList<>(actualItemBids));
    }

    @Test(expected = NullPointerException.class)
    public void getAllBidsForItemShouldShouldThrowNullPointerExceptionWhenItemisNull() {
        bidTracker.getAllBidsForItem(null);
    }

    @Test
    public void getAllItemsUserHasBidOnShouldReturnEmptyListWhenUserHasNotBidOnAnyItems() {
        List<Item> items = bidTracker.getAllItemsUserHasBidOn(user1);
        assertTrue(items.isEmpty());
    }

    @Test
    public void getAllItemsUserHasBidOnShouldReturnCorrectItemListWhenUserHasValidBids() throws InvalidBidException {
        bidTracker.placeBid(new Bid(user1, item1, 10));
        bidTracker.placeBid(new Bid(user1, item2, 10));
        bidTracker.placeBid(new Bid(user2, item1, 11));
        List<Item> actualItemList = bidTracker.getAllItemsUserHasBidOn(user1);
        List<Item> expectedItemList = Arrays.asList(item1, item2);
        assertEquals(expectedItemList, actualItemList);
    }

    @Test(expected = NullPointerException.class)
    public void getAllItemsUserHasBidOnShouldShouldThrowNullPointerExceptionWhenUserisNull(){
        bidTracker.getAllItemsUserHasBidOn(null);
    }

}
